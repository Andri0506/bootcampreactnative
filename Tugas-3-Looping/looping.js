// Soal No 1
console.log("============== No 1 ============")
console.log("LOOPING PERTAMA")
var i = 0;
while (i < 20) {
  i += 2;
  console.log(i + " - " + "I love coding");
} 
console.log("LOOPING KEDUA")
var i = 22;
while (i > 2) {
  i -= 2;
  console.log(i + " - " + "I will become a mobile developer");
}
// Soal No 2
console.log("============== No 2 ============")
var deret = 21;
for(deret = 1; deret < 21; deret++) {
  if(deret % 2 !== 0){
    if(deret % 3 == 0){
      console.log(deret + " - " + 'I Love Coding');
    }else{
      console.log(deret + " - " + 'Santai');
    }
  }else if(deret % 2 == 0){
      console.log(deret + " - " + 'Berkualitas');
  }
}
// Soal No 3
console.log("============== No 3 ============")
var persegiPanjang = '';
    for (var p = 1; p < 5; p++) {
        for (var e = 1; e < 9; e++) {
            persegiPanjang += '#';
        }
        persegiPanjang += '\n';
    }
    console.log(persegiPanjang);
// Soal No 4
console.log("============== No 4 ============")
var segitiga = '';
    for (var s = 1; s <= 8; s++) {
        for (var t = 1; t < s; t++) {
            segitiga += '#';
        }
        segitiga += '\n';
    }
    console.log(segitiga);
// Soal No 5
console.log("============= No 5 =============")
var papanCatur='';
for (var c = 1; c <=8; c++) { 

      if (c%2==0) { 

        for (var t = 1; t <= 8; t++) { 

        if (t%2==0) { 
            papanCatur+='#';
        }else papanCatur+= ' ';
        }
        papanCatur+='\n';  

    }else{ 
 
      for (var t = 1; t <= 8; t++) { 

        if (t%2==0) { 
            papanCatur+=' ';
        }else papanCatur+='#';
        }
        papanCatur+='\n';
    } 
  }
  
  console.log(papanCatur);