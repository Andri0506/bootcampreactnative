console.log("============== No 1 ============")

    function range(stardNum, finishNum){
        var arr = [];

        if(stardNum > finishNum){
            var r = stardNum - finishNum + 1;
            for(var i = 0; i < r; i++){
                arr.push(stardNum - i)
            }
        }else if (stardNum < finishNum){
            var r = finishNum - stardNum + 1;
            for(var i = 0; i < r; i++){
                arr.push(stardNum + i)
            }
        }else if (!stardNum || !finishNum){
            return -1
        }
        return arr
    }
console.log(range(1, 10))
console.log(range())
console.log(range(11, 18))
console.log(range(54, 50))
console.log(range(1))

console.log("============== No 2 ============")

    function rangeWithStep(stardNum, finishNum, step){
        var arr = [];

        if(stardNum > finishNum){
            var d = stardNum;
            for(var i = 0; d >= finishNum; i++){
                arr.push(d)
                d -= step
            }
        }else if (stardNum < finishNum){
            var d = stardNum;
            for(var i = 0; d <= finishNum; i++){
                arr.push(d)
                d += step
            }
        }else if (!stardNum || !finishNum || step){
            return -1
        }
        return arr
    }
console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log(rangeWithStep(29, 2, 4))

console.log("============== No 3 ============")
    function sum(stardNum, finishNum, step){
        var arr2 = [];
        var dis;
        if(!step){
          dis = 1
        }else{
          dis = step
        }
      
        if(stardNum > finishNum){
            var total = stardNum;
            for(var i = 0; total >= finishNum; i++){
                arr2.push(total)
                total -= dis
            }
        }else if (stardNum < finishNum){
            var total = stardNum;
            for(var i = 0; total <= finishNum; i++){
                arr2.push(total)
                total += dis
            }
        }else if (!stardNum && !finishNum && !step){
            return 0
        }else if (stardNum){
            return stardNum
        }
        var t = 0;
        for(var i = 0; i < arr2.length; i++){
          t = t + arr2[i]
        }
        return t
    }
console.log(sum(1, 10))
console.log(sum(5, 50, 2))
console.log(sum(15, 10))
console.log(sum(20, 10, 2))
console.log(sum(1))
console.log(sum(0))

console.log("============== No 4 ============")
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

    function dataHandling(data){
        var dataLength = data.length
            for (var i = 0; i < dataLength; i++){
            var id = "Nomor Id :" + data[i][0]
            var nama = "Nama Lengkap :" + data[i][1]
            var ttl = "TTL :" + data[i][2] + " " + data[i][3]
            var hobi = "Hobi :" + data[i][4]

            console.log(id)
            console.log(nama)
            console.log(ttl)
            console.log(hobi)
            console.log("\n")
        }
    }   

dataHandling(input)

console.log("============== No 5 ============")
    function balikKata(kata){
        var kataBalik = " ";
        for(var i = kata.length-1; i >=0; i--){
        kataBalik += kata[i]
        }
        return kataBalik;
    }
    
    console.log(balikKata("Kasur Rusak"))
    console.log(balikKata("SanberCode"))
    console.log(balikKata("Haji Ijah"))
    console.log(balikKata("racecar"))
    console.log(balikKata("I am Sanbers"))