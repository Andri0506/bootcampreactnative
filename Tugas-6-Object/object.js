console.log("============== NO 1 ===============")
function arrayToObject(arr) {
    if(arr.length <= 0){
      return console.log("")
    }
    for(var i=0; i < arr.length; i++){
      var objectBaru = {}
      var tahunLahir = arr[i][3];
      var now = new Date().getFullYear()
      var newAge;
      if(tahunLahir && now - tahunLahir >0){
        newAge = now - tahunLahir;
      }else{
        newAge = "Invalid Birth Year"
      }
      objectBaru.firstName = arr[i][0];
      objectBaru.lastName = arr[i][1];
      objectBaru.gender = arr[i][2];
      objectBaru.age = newAge;
    
      var consoleText = (i + 1) + " ." + objectBaru.firstName + " " + objectBaru.lastName + " : "
    
      console.log(consoleText)
      console.log(objectBaru)
    }
}
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people)
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 

console.log("============== NO 2 ===============")
function shoppingTime(memberId, money) {
  if(!memberId){
    return "Mohon maaf, toko X hanya berlaku untuk member saja";
  }else if( money < 50000){
    return "Mohon Maaf, uang tidak cukup"
  }else{
    var objectBaru = {}
    var moneyChange = money;
    var purchaseList = [];
    var sepatuStacattu = "Sepatu Stacattu";
    var bajuZoro = "Baju Zoro";
    var bajuHn = "Baju Hn";
    var sweaterUniklooh = "Sweater Uniklooh";
    var casingHandphone = "Casing Handphone";

    var check = 0;
    for(var i = 0; moneyChange >= 50000 && check ==0; i++){
        if(moneyChange >= 1500000){
          purchaseList.push(sepatuStacattu)
          moneyChange -= 1500000
        }else if(moneyChange >= 500000){
          purchaseList.push(bajuZoro)
          moneyChange -= 500000
        }else if(moneyChange >= 250000){
          purchaseList.push(bajuHn)
          moneyChange -= 250000
        }else if(moneyChange >= 175000){
          purchaseList.push(sweaterUniklooh)
          moneyChange -= 175000
        }else if(moneyChange >= 50000){
          for(var j = 0; j <= purchaseList.length; j++){
            if(purchaseList[j]== casingHandphone){
              check += 1;
            }
          }if(check==0){
            purchaseList.push(casingHandphone)
            moneyChange -= 50000
          }else{
            purchaseList.push(casingHandphone)
            moneyChange -= 50000
          }
        }
    }
    objectBaru.memberId = memberId
    objectBaru.money = money
    objectBaru.listPurchased = purchaseList
    objectBaru.changeMoney = moneyChange
    return objectBaru
  } 
}
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); 
console.log(shoppingTime('234JdhweRxa53', 15000)); 
console.log(shoppingTime());

console.log("============== NO 3 ===============")
function naikAngkot(arrPenumpang) {
  rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  var arrOutput = []
  if(arrPenumpang.length <= 0){
    return []
  }

  for(var i = 0; i < arrPenumpang.length; i++){
    var objOutput = {}
    var asal = arrPenumpang [i][1]
    var tujuan = arrPenumpang [i][2]
    var indexAsal;
    var indexTujuan;
    
    for(var j = 0; j < rute.length; j++){
      if(rute[j] == asal){
        indexAsal
      }else if(rute [j]== tujuan){
        indexTujuan = j
      }
    } 
  var bayar = (indexTujuan - indexAsal) * 2000

  objOutput.penumpang = arrPenumpang [i][0]
  objOutput.naikDari = asal
  objOutput.tujuan = tujuan
  objOutput.bayar = bayar
  
  arrOutput.push(objOutput)
  }
  return arrOutput
}
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));