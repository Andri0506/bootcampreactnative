console.log("============== No 1 ============")
const golden = () => (
    console.log("This is Golden!")
)

golden();

console.log("============== No 2 ============")
const newFunction = (firstName, lastName) =>{
    return {
        firstName,lastName,
        fullName(){
            console.log(firstName + " " + lastName)
            return
        }
    }
}

newFunction("Fajar","Supriman").fullName()

console.log("============== No 3 ============")

const objectBaru = {
        firstName: "Muhammad",
        lastName: "Soleh",
        destination: "Hogwarts React Conf",
        occupation: "Deve-wizard Avocado",
        spell: "Vimulus Renderus!!!"
}

const { firstName, lastName, destination, occupation, spell} = objectBaru

console.log(firstName, lastName, destination, occupation, spell)

console.log("============== No 4 ============")

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
console.log(combined)

console.log("============== No 5 ============")

const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet,  
    consectetur adipiscing, elit ${planet} do eiusmod tempor
    incididunt ut labore et dolore magna aliqua. Ut enim
    ad minim veniam`
 
// Driver Code
console.log(before) 