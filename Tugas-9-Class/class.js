console.log("=============== No 1 ====================")
class Animal{
    constructor(nama){
        this._nama = nama;
        this._legs = 4;
        this._cold_blood = false;
    }
    get nama(){
        return this._nama
    }
    get legs(){
        return this._legs
    }
    set legs(amount){
        return this._legs = amount
    }
    get cold_blood(){
        return this._cold_blood
    }
}
var sheep = new Animal ("shaun");
console.log(sheep.nama)
console.log(sheep.legs)
console.log(sheep.cold_blood)

// Release 1
class Ape extends Animal{
    constructor(nama, amount){
        super(nama)
        this._legs = amount
    }
    yell(){
        console.log("Auooooo")
    }
}

class Frog extends Animal{
    constructor(nama){
        super(nama)
    }
    jump(){
        console.log("Hop hop")
    }
}

var sungokong = new Ape ("Kera sakti", 2)
sungokong.yell()
console.log(sungokong.nama)
console.log(sungokong.legs)
console.log(sungokong.cold_blood)

var katak = new Frog("Buduk")
katak.jump()
console.log(katak.nama)
console.log(katak.legs)
console.log(katak.cold_blood)

console.log("=============== No 2 ====================")
class Clock{
    constructor({template}){
        this.template = template;
    }
    reeder(){
        var date = new Date ();
        var hours = date.getHours();
        if(hours < 10) hours = '0' + hours;

        var mins = date.getMinutes()
        if(mins < 10) mins = '0' + mins;

        var secs = date.getSeconds()
        if(secs < 10) secs = '0' + secs;

        var output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs)

        console.log(output)
    }
    stop(){
        clearInterval(this.timer)
    }
    start(){
        this.reeder();
        this.timer = setInterval(()=>this.reeder(), 1000);
    }
}
var clock = new Clock({template: 'h:m:s'});
clock.start();