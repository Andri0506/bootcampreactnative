import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import YoutubeUI from './Tugas/Tugas12/App';
import Login from './Tugas/Tugas-13-Styling/Login';
import Register from './Tugas/Tugas-13-Styling/Register';
import About from './Tugas/Tugas-13-Styling/About';
import Tugas14 from './Tugas/Tugas-14-API-Lifecycle/App';
import Index from './Tugas/Tugas-15-Navigation/index';
import Quiz3 from './Quiz3/index';

export default function App() {
  return (
      // <YoutubeUI />
      // <View style={styles.container}>
      //   <Text>Hello World</Text>
      //   <StatusBar style="auto" />
      // </View>
      //  <Login/>
      // <Register />
      //<About />
      // <Tugas14 />
      // <Index />
      <Quiz3 />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

