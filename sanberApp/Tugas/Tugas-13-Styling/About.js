import React, { Component } from 'react';
import { Alert, Button, TextInput, View, StyleSheet, Image, TouchableOpacity, Text } from 'react-native';

import { AntDesign } from '@expo/vector-icons';
import { EvilIcons } from '@expo/vector-icons';
import { FontAwesome } from '@expo/vector-icons';
import { MaterialIcons } from '@expo/vector-icons';


export default class About extends React.Component {

  render() {
    return (
        <View style={styles.container}>
            <Text style={styles.textLogin}>Tentang Saya</Text>
            <EvilIcons name="user" size={230} color="black" />
            <Text style={styles.textNama}>Andri Alpian</Text>
            <Text style={styles.textJob}>React Native Developer</Text>
            <View style={styles.box} >
              <Text style={styles.textPortofolio}>Portofolio</Text>
                <View style = {styles.lineStyle} />
                <AntDesign name="gitlab" size={24} color="black" style={styles.iconGitlab}/>
                <Text style={styles.textGitlab}>@Andri0506</Text>
                <AntDesign name="github" size={24} color="black" style={styles.iconGithub}/>
                <Text style={styles.textGithub}>@Andri0506</Text>
            </View>
            <View style={styles.box2} >
              <Text style={styles.textContact}>Contact</Text>
                <View style = {styles.lineStyle} />
                <AntDesign name="linkedin-square" size={24} color="black" style={styles.iconIn}/>
                <Text style={styles.textIn}>An dri</Text>
                <FontAwesome name="whatsapp" size={24} color="black" style={styles.iconWa}/>
                <Text style={styles.textWa}>082110022936</Text>
                <MaterialIcons name="email" size={24} color="black" style={styles.iconGmail}/>
                <Text style={styles.textGmail}>an.dri5@gmail.com</Text>
            </View>
        </View>

      );
    } 
  }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  textLogin: {
    padding: 20,
    fontSize: 30,
    color: '#000000',
    marginTop: 80,
    // fontFamily: Roboto,
  },

  textNama: {
    padding: 10,
    fontSize: 30,
    color: '#000000',
    marginTop: 0,
    // fontFamily: Roboto,
  },

  textJob: {
    padding: 10,
    fontSize: 30,
    color: '#000000',
    // fontFamily: Roboto,
  },

  box: {
    width: 350,
    height: 100,
    backgroundColor: '#DFDFDF',
    borderWidth: 2,
    borderColor: '#DFDFDF',
    borderRadius: 10,
    marginTop: 30
  },

  textPortofolio: {
    padding: 10,
    fontSize: 20,
    color: '#000000',
    marginTop: -10
  },

  lineStyle:{
    borderWidth: 0.5,
    borderColor:'black',
    margin:10,
    marginTop: -5,
    borderWidth: 1,
  },

  iconGitlab: {
    marginLeft: 80
  },

  textGitlab: {
    marginLeft: 50
  },

  iconGithub: {
    marginLeft: 220,
    marginTop: -45
  },

  textGithub: {
    marginLeft: 190
  },

  box2: {
    width: 350,
    height: 100,
    backgroundColor: '#DFDFDF',
    borderWidth: 2,
    borderColor: '#DFDFDF',
    borderRadius: 10,
    marginTop: 10
  },

  textContact: {
    padding: 10,
    fontSize: 20,
    color: '#000000',
    marginTop: -10
  },

  iconIn: {
    marginLeft: 35
  },

  textIn: {
    marginLeft: 30
  },

  iconWa: {
    marginLeft: 120,
    marginTop: -45
  },

  textWa: {
    marginLeft: 85
  },

  iconGmail: {
    marginLeft: 240,
    marginTop: -45
  },

  textGmail: {
    marginLeft: 200
  },
});