import React, { Component } from 'react';
import { Alert, Button, TextInput, View, StyleSheet, Image, TouchableOpacity, Text } from 'react-native';


export default class Register extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      username: '',
      password: '',
    };
  }
  
  onLogin() {
    const { username, password } = this.state;

    Alert.alert('Credentials', `${username} + ${password}`);
  }

  render() {
    return (
        <View style={styles.container}>
            <Image source={require('./images/logo.png')} style={styles.imageLogo} />
            <Text style={styles.textLogin}>Register</Text>
            <TextInput
                value={this.state.username}
                onChangeText={(username) => this.setState({ username })}
                placeholder={'Username'}
                style={styles.input}
            />
            <TextInput
                value={this.state.email}
                onChangeText={(email) => this.setState({ email })}
                placeholder={'Email'}
                style={styles.input}
            />
            <TextInput
                value={this.state.password}
                onChangeText={(password) => this.setState({ password })}
                placeholder={'Password'}
                secureTextEntry={true}
                style={styles.input}
            />
            <TextInput
                value={this.state.conpassword}
                onChangeText={(conpassword) => this.setState({ conpassword })}
                placeholder={'Ulangi Password'}
                secureTextEntry={true}
                style={styles.input}
            />
            {/* <Button
                title={'Masuk'}
                style={{width: 98, height: 50}} 
            /> */}
            <TouchableOpacity style={styles.buttonStyle}>
              <Text style={styles.textSignup}>Daftar</Text>
            </TouchableOpacity>
        </View>
      );
    } 
  }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  input: {
    width: 300,
    height: 55,
    padding: 15,
    borderWidth: 2,
    borderColor: 'black',
    borderRadius: 10,
    marginBottom: 10,
    color: 'black',
    backgroundColor: '#ffffff88'
  },

  imageLogo: {
    width: 350,
    marginTop: 10,
    justifyContent: 'center'
  },

  textLogin: {
    padding: 20,
    fontSize: 30,
    color: '#003366',
    // font-family: Roboto;
  },

  buttonStyle:{
    backgroundColor:'#3EC6FF',
    padding: 5,
    paddingLeft: 10,
    paddingRight: 10,
    borderRadius: 10,
    marginTop: 30,
    width:300
  },
  textSignup: {
    fontSize: 16,
    textAlign: 'center',
    margin: 10,
    color:'white'
  },
  instructions: {
    textAlign: 'center',
    color: '#003366',
    marginBottom: 5,
    marginTop:10
  }
  
});